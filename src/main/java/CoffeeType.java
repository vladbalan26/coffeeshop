/**
 * Describes the type of coffee that we're going to serve.
 */
public enum CoffeeType {
    Espresso(5, 0),
    Americano(10, 0),
    Latte(7, 200);


    private int requiredBeans;
    private int requiredMilk;

    CoffeeType(int requiredBeans, int requiredMilk) {
        this.requiredBeans = requiredBeans;
        this.requiredMilk = requiredMilk;
    }

    public int getRequiredBeans() {
        return requiredBeans;
    }

    public int getRequiredMilk() {
        return requiredMilk;
    }
}
